package com.example.alekskrause.notes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.robohorse.pagerbullet.PagerBullet;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PagerBullet viewPager = (PagerBullet) findViewById(R.id.pager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        // Add Fragments to adapter one by one
        adapter.addFragment(new BlankFragment(), "FRAG1");
        adapter.addFragment(new BlankFragment(), "FRAG2");
        viewPager.setAdapter(adapter);
    }
}
