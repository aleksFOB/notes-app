package com.example.alekskrause.notes;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aleksKrause on 18.01.2018.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private ArrayList<String> dataSet;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public CardView cardView;
        public EditText edNote;
        public ImageView imgDelete;

        public MyViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.card_view);
            edNote = (EditText) itemView.findViewById(R.id.ed_note);
            imgDelete = (ImageView) itemView.findViewById(R.id.img_delete);
        }
    }

    public MyAdapter(ArrayList<String> notes) {
        dataSet = notes;
    }

    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyAdapter.MyViewHolder holder, int position) {
        holder.edNote.setText(dataSet.get(position));
        holder.edNote.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int posForTextChange = holder.getAdapterPosition();
                if (charSequence.length() != 0 || posForTextChange!=dataSet.size()) {
                    dataSet.set(posForTextChange, charSequence.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = holder.getAdapterPosition();
                if(position==-1) return;
                dataSet.remove(position);
                notifyItemRemoved(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public void addNew(String newItemString) {
        int insertionPos;
        if(dataSet.size()>0)insertionPos = dataSet.size();
        else insertionPos = 0;
        dataSet.add(insertionPos, newItemString);
        notifyItemInserted(insertionPos);

    }


    public ArrayList<String> getDataSet() {
        return dataSet;
    }
}
