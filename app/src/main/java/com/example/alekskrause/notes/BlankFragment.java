package com.example.alekskrause.notes;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aleksKrause on 18.01.2018.
 */

public class BlankFragment extends Fragment {

    private String OUT_DATA_SET = "out_data_set";
    private MyAdapter adapter;

    public BlankFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_one, container, false);
        RecyclerView rv = (RecyclerView) rootView.findViewById(R.id.rv_recycler_view);
        rv.setHasFixedSize(true);

        if (savedInstanceState!=null && savedInstanceState.containsKey(OUT_DATA_SET)) {
            adapter = new MyAdapter(savedInstanceState.getStringArrayList(OUT_DATA_SET));
        } else adapter = new MyAdapter(getStubStrings());

        rv.setAdapter(adapter);
        rootView.findViewById(R.id.rl_add_new).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapter.addNew(getString(R.string.edit_me));
            }
        });

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        return rootView;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putStringArrayList(OUT_DATA_SET, adapter.getDataSet());
        super.onSaveInstanceState(outState);
    }

    //add strings if want in initial layout here
    private ArrayList<String> getStubStrings() {
        ArrayList<String> strings = new ArrayList<>();
        return strings;
    }
}
